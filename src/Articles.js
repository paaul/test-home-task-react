import React from 'react';
export default class Articles extends React.Component {

    constructor(params) {
        super(params);
        this.state = {
            articles: [
        {id: '123', author:'John Smith', title: 'Збільшились надої корів', desc: 'Any description, isperty, dedicated to obsessively profiling startups, reviewing new Internet products, and breaking tech news.'},
        {id: '12', author:'John Smith',title: 'Чому я люблю React', desc: 'Any description '},
        {id: '2', author:'John Smith',title: 'JS і Java, що краще ?', desc: 'Any description'},
        {id: '44', author:'John Smith',title: 'Знайшли воду на марсі!', desc: 'Any description'},
        {id: '11', author:'John Smith', title: 'Cats and Dogs', desc: 'Any description'},
    ]}}

    render () {

        const artLes = this.state.articles.map(function (article, index) {
            return (
                <div key={index}>
                    <p className="articles__title">{article.title}</p>
                    <div>
                        <img className="image" src={"http://www.pvhc.net/img114/qmfzhfnbljgfgvybitss.png"} alt=""/>
                        <span className="articles__author" >{article.author}</span>
                    </div>

                    <p className="articles__desc">{article.desc}</p>
                </div>
            )
        })


        return(
        <div>
            {artLes}
        </div>
    );
}



}


