import React from 'react';
import { Link, Route, Switch } from 'react-router-dom';
import Contacts from './Contacts';
import Root from './Root';
import Articles from './Articles';


class App extends React.Component {
  render() {
    return (
        <div className="app">
        <header>
            <span>navigation -> </span>
            <Link to="/">Root</Link>
            <span> | </span>
            <Link to="/articles">Articles</Link>
            <span> | </span>
            <Link to="/contacts">Contacts</Link>
        </header>
        <Switch>
            <Route exact path="/" component={Root}/>
            <Route exact path="/articles" component={Articles}/>
            <Route exact path="/contacts" component={Contacts}/>
        </Switch>
        </div>);
  }
}

export default App;