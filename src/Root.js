import React from 'react';


export default class Root extends React.Component {

    render() {
        return(
            <div>
                <p>This is home application</p>
                <p>And this is root component</p>
                <p>This is first react homework</p>
                <p>i'll do it asap, cause it's important!</p>
                <br/>
                <p>software engineer, not coder</p>
            </div>
        );
    }
}