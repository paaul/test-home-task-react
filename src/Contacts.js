import React from 'react';
import ContactItem from "./ContactItem";


class Contacts extends React.Component {

    constructor(params){
        super(params);
        this.state ={
     contacts: [
        {unique:'0001', name: 'Mark', phone: '095-223-24-12'},
        {unique:'0031',name: 'Stan', phone: '093-243-41-33'},
        {unique:'5031',name: 'Eugen', phone: '095-555-61-77'},
        {unique:'1021',name: 'Vlada', phone: '066-777-21-66'},
        {unique:'2322',name: 'Katya', phone: '098-111-25-16'},
        {unique:'2322',name: 'Katya', phone: '098-111-25-16'},
    ]}}

    render() {
        const conts = this.state.contacts.map((contact) => <ContactItem key={contact.unique} name={contact.name} phone={contact.phone} />);

        return (
            <div className="contacts">
                <h1>Contacts</h1>
                {conts}
            </div>

        )
     }
}
export default Contacts;