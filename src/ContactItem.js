import React from 'react';

export default class ContactItem extends React.Component {

    render() {
        return(
            <div className="contactBorder">
                <span>{this.props.name}</span>
                <span className="phone">{this.props.phone}</span>
                <br/>
            </div>

        );
    }

}